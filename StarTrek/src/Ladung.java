/**
 * 
 * @author Junia.Walther
 * @version 4.2.1
 * @since 11.05.2022
 * 
 */
public class Ladung {
	
	private String ladungsart;
	private int anzahl;
	
	//Konstruktoren
	public Ladung() {
		this.ladungsart = "unbekannt";
		this.anzahl = 0;
	}
	public Ladung(String ladungsart, int anzahl) {
		this.ladungsart = ladungsart;
		this.anzahl = anzahl;
	}
	
	// Getter und Setter
	/**
	 * 
	 * @param
	 * @return String
	 * 
	 */
	public String getLadungsart() {
		return ladungsart;
	}
	/**
	 * 
	 * @param ladungsart
	 * @return void
	 * 
	 */
	public void setLadungsart(String ladungsart) {
		this.ladungsart = ladungsart;
	}
	/**
	 * 
	 * @param
	 * @return int
	 * 
	 */
	public int getAnzahl() {
		return anzahl;
	}
	/**
	 * 
	 * @param anzahl
	 * @return void
	 * 
	 */
	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}
	
	//andere Methoden
	/**
	 * 
	 * @param
	 * @return String
	 * 
	 */
	public String toString() {
		return this.ladungsart + ", Stueckzahl " + this.anzahl;
	}
}
