import java.util.*;
/**
 * 
 * @author Junia.Walther
 * @version 4.2.1
 * @since 11.05.2022
 * 
 */
public class Simulation {

	
	public static void main(String[] args) {
		
		Ladung lk1 = new Ladung("Ferengi Schneckensaft", 200);  //lk = Ladung Klingonene
		Ladung lk2 = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung lr1 = new Ladung("Borg-Schrott", 5);  //lr = Ladung Romulaner
		Ladung lr2 = new Ladung("Rote Materie", 2);
		Ladung lr3 = new Ladung("Plasma-Waffe", 50);
		Ladung lv1 = new Ladung("Forschungssonde", 35); //lv = Ladung Vulkanier
		Ladung lv2 = new Ladung("Photonentorpedo", 3);
		
		Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 100, 100, 100, 100, 1, 2);
		Raumschiff romulaner = new Raumschiff("IRW Khazara", 100, 100, 100, 100, 2, 2);
		Raumschiff vulkanier = new Raumschiff("Ni'Var", 80, 80, 100, 50, 0, 5);
		
		
		
		klingonen.beladen(lk1);
		klingonen.beladen(lk2);
		romulaner.beladen(lr1);
		romulaner.beladen(lr2);
		romulaner.beladen(lr3);
		vulkanier.beladen(lv1);
		vulkanier.beladen(lv2);
		
		klingonen.photonentorpedosAbschiessen(romulaner);
		romulaner.phaserkanonenAbschiessen(klingonen);
		vulkanier.nachrichtSenden("Gewalt ist nicht logisch");
		klingonen.zustandRaumschiff();
		klingonen.ausgabeLadungsverzeichnis();
		vulkanier.reperaturAuftrag(true, true, true, 5);
		vulkanier.photonentorpedoesLaden(3);
		vulkanier.aufraeumenLadungsverzeichnis();
		klingonen.photonentorpedosAbschiessen(romulaner);
		klingonen.photonentorpedosAbschiessen(romulaner);
		klingonen.zustandRaumschiff();
		klingonen.ausgabeLadungsverzeichnis();
		romulaner.zustandRaumschiff();
		romulaner.ausgabeLadungsverzeichnis();
		vulkanier.zustandRaumschiff();
		vulkanier.ausgabeLadungsverzeichnis();
		System.out.println(klingonen.getBroadcastKommunikator());
		System.out.println(romulaner.getBroadcastKommunikator());
		System.out.println(vulkanier.getBroadcastKommunikator());
	}

}
