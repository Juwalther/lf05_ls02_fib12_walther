import java.util.*;
/**
 * 
 * @author Junia.Walther
 * @version 4.2.1
 * @since 11.05.2022
 * 
 */
public class Raumschiff{
	
	private String schiffname;
	private double energieversorgung;
	private double schutzschilde;
	private double lebenserhaltungssysteme;
	private double huelle;
	private int photonentorpedos;
	private int reperaturdroiden;
	private ArrayList<String> broadcastKommunikator = new ArrayList();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList();
	
	//Konstruktoren
	public Raumschiff() {
		this.schiffname = "unbekannt";
		this.energieversorgung = 0;
		this.schutzschilde = 0;
		this.lebenserhaltungssysteme = 0;
		this.energieversorgung = 0;
		this.huelle = 0;
		this.photonentorpedos = 0;
		this.reperaturdroiden = 0;
	}
	public Raumschiff (String schiffname, double energieversorgung, double schutzschilde, double lebenserhaltungssysteme, double huelle, int photonentorpedos, int reperaturdroiden) {
		this.schiffname = schiffname;
		this.energieversorgung = energieversorgung;
		this.schutzschilde = schutzschilde;
		this.lebenserhaltungssysteme = lebenserhaltungssysteme;
		this.energieversorgung = energieversorgung;
		this.huelle = huelle;
		this.photonentorpedos = photonentorpedos;
		this.reperaturdroiden = reperaturdroiden;
	}

	//Getter und Setter
	/**
	 * 
	 * @param
	 * @return String
	 * 
	 */
	public String getSchiffname() {
		return schiffname;
	}
	/**
	 * 
	 * @param schiffname
	 * @return void
	 * 
	 */
	public void setSchiffname(String schiffname) {
		this.schiffname = schiffname;
	}
	/**
	 * 
	 * @param
	 * @return double
	 * 
	 */
	public double getEnergieversorgung() {
		return energieversorgung;
	}
	/**
	 * 
	 * @param energieversorgung
	 * @return void
	 * 
	 */
	public void setEnergieversorgung(double energieversorgung) {
		this.energieversorgung = energieversorgung;
	}
	/**
	 * 
	 * @param
	 * @return double
	 * 
	 */
	public double getSchutzschilde() {
		return schutzschilde;
	}
	/**
	 * 
	 * @param schutzschilde
	 * @return void
	 * 
	 */
	public void setSchutzschilde(double schutzschilde) {
		this.schutzschilde = schutzschilde;
	}
	/**
	 * 
	 * @param
	 * @return double
	 * 
	 */
	public double getLebenserhaltungssysteme() {
		return lebenserhaltungssysteme;
	}
	/**
	 * 
	 * @param lebernserhaltungssysteme
	 * @return void
	 * 
	 */
	public void setLebenserhaltungssyteme(double lebenserhaltungssysteme) {
		this.lebenserhaltungssysteme = lebenserhaltungssysteme;
	}
	/**
	 * 
	 * @param
	 * @return double
	 * 
	 */
	public double getHuelle() {
		return huelle;
	}
	/**
	 * 
	 * @param huelle
	 * @return void
	 * 
	 */
	public void setHuelle(double huelle) {
		this.huelle = huelle;
	}
	/**
	 * 
	 * @param
	 * @return int
	 * 
	 */
	public int getPhotonentorpedos() {
		return photonentorpedos;
	}
	/**
	 * 
	 * @param photonentorpedos
	 * @return void
	 * 
	 */
	public void setPhotonentorpedos(int photonentorpedos) {
		this.photonentorpedos = photonentorpedos;
	}
	/**
	 * 
	 * @param
	 * @return int
	 * 
	 */
	public int getReperaturdroiden() {
		return reperaturdroiden;
	}
	/**
	 * 
	 * @param reperaturdroiden
	 * @return void
	 * 
	 */
	public void setReperaturdroiden(int reperaturdroiden) {
		this.reperaturdroiden = reperaturdroiden;
	}
	/**
	 * 
	 * @param
	 * @return ArrayList<String>
	 * 
	 */
	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}
	/**
	 * 
	 * @param broadcastKommunikator
	 * @return void
	 * 
	 */
	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}
	/**
	 * 
	 * @param
	 * @return ArrayList<Ladung>
	 * 
	 */
	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}
	/**
	 * 
	 * @param ladungsverzeichnis
	 * @return void
	 * 
	 */
	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
	/**
	 * 
	 * @param zielRaumschiff
	 * @return void
	 * 
	 */
	//weitere Methoden
	public void photonentorpedosAbschiessen (Raumschiff zielRaumschiff) {
		if (photonentorpedos<=0) {
			nachrichtSenden("-=*Click*=-");
		}
		else {
			photonentorpedos = photonentorpedos - 1;
			nachrichtSenden("Photonentorpedo abgeschossen");
			zielRaumschiff.trefferVermerken(zielRaumschiff);
		}
	}
	/**
	 * 
	 * @param zielRaumschiff
	 * @return void
	 * 
	 */
	
	public void phaserkanonenAbschiessen (Raumschiff zielRaumschiff) {
		if (energieversorgung<=50) {
			nachrichtSenden("-=*Click*=-");
		}
		else {
			energieversorgung = energieversorgung - 50;
			nachrichtSenden("Phaserkanone abgeschossen");
			zielRaumschiff.trefferVermerken(zielRaumschiff);
		}
	}
	/**
	 * 
	 * @param ziel
	 * @return void
	 * 
	 */
	private void trefferVermerken(Raumschiff ziel) {
		System.out.println(schiffname + " wurde getroffen");
		int trefferAnzahl = 0;
		trefferAnzahl = trefferAnzahl + 1;
		
		genommenerSchaden(trefferAnzahl, 50);

	}
	/**
	 * 
	 * @param treffer, ausmassSchaden
	 * @return void
	 * 
	 */
	private void genommenerSchaden (int treffer, double ausmassSchaden) {
		for (int hits=treffer; hits>0; hits--) {
		if (schutzschilde>50) {
			schutzschilde = schutzschilde -ausmassSchaden;
		}
		else {
			schutzschilde = 0;
			huelle = huelle - ausmassSchaden;
			energieversorgung = energieversorgung - ausmassSchaden;
			if (huelle >= 0) {
				lebenserhaltungssysteme = 0;
				nachrichtSenden("Die Lebenserhaltungssysteme der Raumschiffes " + schiffname + " wurden vollstaendig zerstoert");
			}
		}
		}
	}
	/**
	 * 
	 * @param nachricht
	 * @return void
	 * 
	 */
	
	public void nachrichtSenden (String nachricht) {
		broadcastKommunikator.add(nachricht);
	}
	/**
	 * 
	 * @param aufzuladeneLadung
	 * @return void
	 * 
	 */
	
	public void beladen (Ladung aufzuladeneLadung) {
		ladungsverzeichnis.add(aufzuladeneLadung);
	}
	/**
	 * 
	 * @param
	 * @return ArrayListyString>
	 * 
	 */
	public ArrayList<String> logbuchZurueckgeben() {
        return broadcastKommunikator;
	}
	/**
	 * 
	 * @param anzahlTorpedos
	 * @return void
	 * 
	 */
	public void photonentorpedoesLaden (int anzahlTorpedos) {
		for (int i = 0; i < ladungsverzeichnis.size(); i++) {
			Ladung l= ladungsverzeichnis.get(i);
			String art = l.getLadungsart();
			int anzahl = l.getAnzahl();
		if (art.equals("Photonentorpedo")) {
		 if (anzahl<anzahlTorpedos) {
			 l.setAnzahl(0);
			 setPhotonentorpedos(anzahl);
			 System.out.println(anzahl + " Photonentorpedo(s) eingesetzt");
		 }
		 else {
			 l.setAnzahl(anzahl-anzahlTorpedos);
			 setPhotonentorpedos(anzahlTorpedos);
			 System.out.println(anzahlTorpedos + " Photonentorpedo(s) eingesetzt");
		 }
        }
		}
	}
	/**
	 * 
	 * @param schutzschild, energie, schiffhuelle, anzahlAndroiden
	 * @return void
	 * 
	 */
	public void reperaturAuftrag(boolean schutzschild, boolean energie, boolean schiffhuelle, int anzahlAndroiden) {
		int count = 0;
		double zufall = Math.random()*101;
		double Reperatur;
		int EndWert;
		if (schutzschild=true) {
			count=count +1;
		}
		if (energie=true) {
			count=count +1;
		}
		if (schiffhuelle=true) {
			count=count +1;
		}
		
		if (anzahlAndroiden>reperaturdroiden) {
		Reperatur = (zufall*reperaturdroiden)/count;
		EndWert = (int) Math.round(Reperatur);
		}
		else {
		Reperatur = (zufall*anzahlAndroiden)/count;	
		EndWert = (int) Math.round(Reperatur);
		}
		schutzschilde = schutzschilde + EndWert;
		huelle = huelle + EndWert;
		energieversorgung = energieversorgung + EndWert;
	}
	/**
	 * 
	 * @param
	 * @return void
	 * 
	 */
	public void zustandRaumschiff () {
		System.out.println("Zustand des Raumschiffes " + schiffname + ":");
		System.out.println("Die Energieversorung liegt bei " + energieversorgung + "%");
		System.out.println("Die Schutzschilde liegen bei " + schutzschilde + "%");
		System.out.println("Die Lebenserhaltungssysteme liegen bei " + lebenserhaltungssysteme + "%");
		System.out.println("Die Huelle liegt bei " + huelle + "%");
		System.out.println("Die Anzahl der Photonentorpedos liegt bei " + photonentorpedos);
		System.out.println("Die Anzahl der Reperaturdroiden liegt bei " + reperaturdroiden);
	}
	/**
	 * 
	 * @param
	 * @return void
	 * 
	 */
	public void ausgabeLadungsverzeichnis() {
		System.out.println();
		System.out.println("Das Raumschiff " + schiffname + " besitzt folgende Ladung:");
		for (int i = 0; i<ladungsverzeichnis.size(); i++) {
			System.out.println(ladungsverzeichnis.get(i));
		}
		System.out.println();
	}
	/**
	 * 
	 * @param
	 * @return void
	 * 
	 */
	public void aufraeumenLadungsverzeichnis() {
		for (int i = 0; i < ladungsverzeichnis.size(); i++) {
			Ladung l= ladungsverzeichnis.get(i);
			double anzahl = l.getAnzahl();
		if (anzahl == 0) {
			ladungsverzeichnis.remove(i);
		}
		}		
	}
	/**
	 * 
	 * @param neueLadung
	 * @return void
	 * 
	 */
	public void addLadung (Ladung neueLadung) {
		ladungsverzeichnis.add(neueLadung);
	}

}
